package com.example.camerademo.helper;

import android.app.Activity;
import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.camerademo.R;
import com.example.camerademo.utils.CameraUtil;
import com.example.camerademo.utils.Constants;
import com.example.camerademo.utils.ImageUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback,Camera.PreviewCallback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Activity activity;
    //
    private List<Camera.Size> mSupportedPreviewSizes;
    private Camera.Size mPreviewSize;
    private List<PreviewModel> previewSizes = new ArrayList<>();

    //
    private byte[] capturedBytes;
    private int cameraId;

    //
    private int mRatioWidth = 0;
    private int mRatioHeight = 0;



    public CameraPreview(Context context, Camera camera,Activity activity) {
        super(context);
        this.activity =activity;
        mCamera = camera;


        // supported preview sizes
        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        previewSizes.clear();
        for(Camera.Size str: mSupportedPreviewSizes){
            Log.e("TAG", str.width + "/" + str.height);
            previewSizes.add(new PreviewModel(str.width,str.height));
        }


        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("TAG", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);

            Camera.Parameters params = mCamera.getParameters();
            if(previewSizes != null && previewSizes.size()>0){
                params.setPreviewSize(previewSizes.get(0).getWidth(), previewSizes.get(0).getHeight());
            }else {
                params. setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            }
            Log.e("TAG","mPreviewSize.width = "+mPreviewSize.width);
            Log.e("TAG","mPreviewSize.height = "+mPreviewSize.height);
            setPreviewSize(params, w, h);
            setSupportedAutoFocus(params);
            mCamera.setParameters(params);

            mCamera.setPreviewCallback(this);

            mCamera.startPreview();
            startFaceDetection();
        } catch (Exception e){
            Log.d("TAG", "Error starting camera preview: " + e.getMessage());
        }
    }



    public void refreshCamera(Camera camera)
    {
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        setCamera(camera);
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

            mCamera.setPreviewCallback(this);

            startFaceDetection();
        } catch (Exception e) {
            Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }

        if (mPreviewSize!=null) {
            float ratio;
            if(mPreviewSize.height >= mPreviewSize.width)
                ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
            else
                ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

            // One of these methods should be used, second method squishes preview slightly
            setMeasuredDimension(width, (int) (width * ratio));
            //        setMeasuredDimension((int) (width * ratio), height);
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }

    public void setCamera(Camera camera) {
        //method to set a camera instance
        mCamera = camera;
    }

    private void startFaceDetection() {
        // Try starting Face Detection
        Camera.Parameters params = mCamera.getParameters();

        // start face detection only *after* preview has started
        if (params.getMaxNumDetectedFaces() > 0){
            // camera supports face detection, so can start it:
            mCamera.startFaceDetection();
            mCamera.setFaceDetectionListener(new MyFaceDetectionListener());

        }
    }

    public List<PreviewModel> getPreviewSize(){
        return previewSizes;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        this.capturedBytes = data;
    }


    public static class MyFaceDetectionListener implements Camera.FaceDetectionListener {

        @Override
        public void onFaceDetection(Camera.Face[] faces, Camera camera) {
            if (faces.length > 0){
                Log.d("FaceDetection", "face detected: "+ faces.length +
                        " Face 1 Location X: " + faces[0].rect.centerX() +
                        "Y: " + faces[0].rect.centerY() );
            }
        }
    }

    public void setCameraId(int cameraId){
        this.cameraId = cameraId;
    }

    public Bitmap getBitmap() {
        try {
            int width = mCamera.getParameters().getPreviewSize().width;
            int height = mCamera.getParameters().getPreviewSize().height;
            int previewFormat = mCamera.getParameters().getPreviewFormat();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            YuvImage yuvImage = new YuvImage(capturedBytes, previewFormat, width, height, null);
            yuvImage.compressToJpeg(new Rect(0, 0, width, height), 100, out);
            byte[] imageBytes = out.toByteArray();
            Bitmap bitmap = ImageUtil.decodeByteArray(imageBytes, width, height);
            boolean shouldFlip = cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT;
            Bitmap rotatedBitmap = ImageUtil.rotateBitmapTo90Degree(bitmap, shouldFlip);
            /*rotatedBitmap = ImageUtil.rotateBitmapTo270Degree(bitmap, shouldFlip);
            rotatedBitmap = ImageUtil.rotateBitmapTo180Degree(bitmap, shouldFlip);*/
            return rotatedBitmap != null ? rotatedBitmap : bitmap;
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }


    public void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private void setSupportedAutoFocus(@NonNull Camera.Parameters parameters) {
        List<String> supportedFocusList = parameters.getSupportedFocusModes();

        if (supportedFocusList.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

        else if (supportedFocusList.contains(Camera.Parameters.FOCUS_MODE_AUTO))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
    }

    private void setPreviewSize(@NonNull Camera.Parameters parameters, int width, int height) {
        Camera.Size fullScreenSize = ImageUtil.chooseOptimalSize(parameters.getSupportedPictureSizes(), width, height);
        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point);
        Camera.Size bestSize = ImageUtil.chooseOptimalSize(parameters.getSupportedPreviewSizes(), width, height, point.x, point.y, fullScreenSize);
        parameters.setPreviewSize(bestSize.width, bestSize.height);
        setAspectRatio(bestSize.height, bestSize.width);
    }

    private int getCorrectCameraOrientationForFace() {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT, info);
        return (360 - info.orientation) % 360;
    }

    public void setAspectRatio(int width, int height) {
        if (width >= 0 && height >= 0) {
            mRatioWidth = width;
            mRatioHeight = height;
            requestLayout();
        }
    }

    public void startCamera() {
        try {
            mCamera = CameraUtil.getCameraInstance(cameraId);
            int orientation = Constants.PORTRAIT_ORIENTATION;
            if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
                orientation = getCorrectCameraOrientationForFace();
            mCamera.setDisplayOrientation(orientation);
            this.mHolder = getHolder();
            this.mHolder.addCallback(this);
            this.mCamera.setPreviewDisplay(this.mHolder);
            this.mCamera.setPreviewCallback(this);
            this.mCamera.startPreview();
        } catch (Exception e) {
        }
    }
}
