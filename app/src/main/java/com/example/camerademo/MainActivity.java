package com.example.camerademo;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.camerademo.helper.CameraPreview;
import com.example.camerademo.helper.CircularRevealTransition;
import com.example.camerademo.utils.FileUtil;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class MainActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback picture;

    private boolean cameraFront;
    private int cameraId;
    private int zoomLevel;

    private FrameLayout preview;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private TextView tvZoom,tv1x,tv2x,tv3x,tv4x,tv5x;
    private LinearLayout llZoom,llZoomLavel;
    private ImageView ivPreviewSizes;

    private PhotoView photoViewShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preview = findViewById(R.id.camera_preview);
        llZoom = findViewById(R.id.llZoom);
        llZoomLavel = findViewById(R.id.llZoomLavel);
        tvZoom = findViewById(R.id.tvZoom);
        tv1x = findViewById(R.id.tv1x);
        tv2x = findViewById(R.id.tv2x);
        tv3x = findViewById(R.id.tv3x);
        tv4x = findViewById(R.id.tv4x);
        tv5x = findViewById(R.id.tv5x);
        ivPreviewSizes = findViewById(R.id.ivPreviewSizes);
        photoViewShow = findViewById(R.id.photoViewShow);
        checkPermission();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermission() {
        requestPermissions(new String[] {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO}, 1);
    }

    private void init()
    {

        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera,MainActivity.this);
        preview.addView(mPreview);

        // Create an instance of Camera
        //mCamera = getCameraInstance();
        chooseCamera();
        mCamera.setDisplayOrientation(90);
        picture = getPictureCallback();


        Log.e("TAG","mCamera.getParameters().getZoom() ="+mCamera.getParameters().getZoom());

        Camera.Parameters params = mCamera.getParameters();
        int zoom = params.getMaxZoom();
        zoomLevel = 0;


        ImageButton captureButton =  findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        mCamera.takePicture(null, null, picture);
                    }
                }
        );

        ImageView button_rotate =  findViewById(R.id.button_rotate);
        button_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    //release the old camera instance
                    //switch camera, from the front and the back and vice versa
                    mPreview.releaseCamera();
                    chooseCamera();
                }
            }
        });

        ImageView ivFilter = findViewById(R.id.ivFilter);
        final HorizontalScrollView horizontalScrollView = findViewById(R.id.hscFilterLayout);
        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(horizontalScrollView.getVisibility() == View.VISIBLE) {
                    horizontalScrollView.setVisibility(View.GONE);
                } else {
                    horizontalScrollView.setVisibility(View.VISIBLE);
                }
            }
        });

        tvZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setVisbilityGoForZoomLavel(Integer.MIN_VALUE);
            }
        });

        tv1x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvZoom.setText(tv1x.getText());
                setVisbilityGoForZoomLavel(0);
            }
        });

        tv2x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvZoom.setText(tv2x.getText());
                setVisbilityGoForZoomLavel(1);
            }
        });

        tv3x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvZoom.setText(tv3x.getText());
                setVisbilityGoForZoomLavel(2);
            }
        });

        tv4x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvZoom.setText(tv4x.getText());
                setVisbilityGoForZoomLavel(3);
            }
        });

        tv5x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvZoom.setText(tv5x.getText());
                setVisbilityGoForZoomLavel(4);
            }
        });

        ivPreviewSizes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPreviewDialog();
            }
        });
    }

    private void openPreviewDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Preview Size");

        // add a list
        String[] preview = new String[mPreview.getPreviewSize().size()];

        for(int i = 0;i<mPreview.getPreviewSize().size();i++) {
            preview[i] = mPreview.getPreviewSize().get(i).getWidth()+"*"+mPreview.getPreviewSize().get(i).getHeight();
        }

        builder.setItems(preview, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPreviewSize(mPreview.getPreviewSize().get(which).getWidth(), mPreview.getPreviewSize().get(which).getHeight());
                mCamera.setParameters(parameters);
                mCamera.startPreview();
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setVisbilityGoForZoomLavel(int zoom){
        Transition transition = new CircularRevealTransition();
        transition.setDuration(600);
        transition.addTarget(R.id.llZoom);

        TransitionManager.beginDelayedTransition(llZoomLavel, transition);
        llZoom.setVisibility(llZoom.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE );

        tvZoom.setVisibility(tvZoom.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

        if(zoom != Integer.MIN_VALUE){
            Camera.Parameters params = mCamera.getParameters();

            params.setZoom((params.getMaxZoom()/4)*zoom);
            zoomLevel = zoomLevel*zoom;
            mCamera.setParameters(params);
        }

    }


    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }


    private Camera.PictureCallback getPictureCallback(){
        Camera.PictureCallback mPicture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                //Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                //photoViewShow.setVisibility(View.VISIBLE);
                //Bitmap x = mPreview.getBitmap1(data);
                //Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                //File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                File pictureFile = FileUtil.createImageFile();
                if (pictureFile != null)
                    makePicturePublic(pictureFile);

                if (pictureFile != null && FileUtil.saveImageInFile(mPreview.getBitmap(), pictureFile)) {
                    //sendSuccessResult(pictureFile);
                    //photoViewShow.setImageBitmap(mPreview.getBitmap());
                    return;
                } else {
                    //sendFailureResultToClient();
                }

                //File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                if (pictureFile == null){
                    Log.d("TAG", "Error creating media file, check storage permissions");
                    return;
                }

                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    fos.write(data);
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.d("TAG", "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.d("TAG", "Error accessing file: " + e.getMessage());
                }
            }
        };
        return mPicture;
    }

    public void makePicturePublic(@NonNull File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }



    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                picture = getPictureCallback();

                /*mPreview = new CameraPreview(this, mCamera);
                preview.addView(mPreview);*/

                focus();
            }
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                mCamera = Camera.open(cameraId);
                //mCamera = Camera.open();
                mCamera.setDisplayOrientation(90);
                picture = getPictureCallback();

                /*mPreview = new CameraPreview(this, mCamera);
                preview.addView(mPreview);*/

                focus();
            }
        }

        mPreview.setCameraId(cameraId);
    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }




    @Override
    protected void onPause() {
        super.onPause();
        if(mPreview != null)
        mPreview.releaseCamera();
    }



    @Override
    protected void onResume() {
        super.onResume();
        if(mCamera == null && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            mCamera = Camera.open(cameraId);
            //mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            picture = getPictureCallback();

            mPreview = new CameraPreview(this, mCamera,MainActivity.this);
            preview.addView(mPreview);

            focus();
            Log.d("nu", "null");
        }else {
            if (mPreview != null)
                mPreview.startCamera();
            Log.d("nu","no null");
        }
    }


    private void focus(){

        mPreview.refreshCamera(mCamera);

        Camera.Parameters params = mCamera.getParameters();
        /*params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        Log.e("TAG","zoomLevel = "+zoomLevel);*/

        params.setZoom(zoomLevel);
        mCamera.setParameters(params);

    }


    public void colorEffectFilter(View v){
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            switch (v.getId()) {
                case R.id.rlNone:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_NONE);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlAqua:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_AQUA);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlBlackBoard:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_BLACKBOARD);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlMono:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_MONO);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlNegative:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_NEGATIVE);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlPosterized:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_POSTERIZE);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlSepia:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_SEPIA);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlSolarized:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_SOLARIZE);
                    mCamera.setParameters(parameters);
                    break;
                case R.id.rlWhiteBoard:
                    parameters.setColorEffect(Camera.Parameters.EFFECT_WHITEBOARD);
                    mCamera.setParameters(parameters);
                    break;
            }
        }catch (Exception ex){
            Log.d("TAG",ex.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED){
            init();
        }else {
            requestPermissions(new String[] {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO}, 1);
        }
    }

    @Override
    public void onBackPressed() {
        boolean shouldBack = photoViewShow.getVisibility() != View.VISIBLE;
        if (!shouldBack)
            onClickRetry();
        else
        super.onBackPressed();
    }

    private void onClickRetry() {
        photoViewShow.setVisibility(View.GONE);
    }
}

