package com.example.camerademo.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by VINOD KUMAR on 12/26/2018.
 */
public final class FileUtil {

    private FileUtil() {
    }


    public static File createImageFile() {
        File storageDir = getDefaultPublicPictureStorage();
        return storageDir != null ? createImageFile(storageDir.getAbsolutePath()) : null;
    }


    public static File createImageFile(String path) {
        return createImageFile(path, getDefaultImageFileName());
    }


    public static File createImageFile(String path, String imageFileName) {
        File root = new File(path);
        return (root.exists() || root.mkdirs()) ? new File(path + imageFileName) : null;
    }

    public static boolean saveImageInFile(Bitmap bitmap, File file) {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            //getImageOrientation(file.getPath(),bitmap);

            return true;
        } catch (IOException e) {
            return false;
        } catch (OutOfMemoryError error) {
            return false;
        }
    }

    private static String getDefaultImageFileName() {
        String timeStamp = new SimpleDateFormat(Constants.DEFAULT_DATE_TIME_FORMAT, Locale.US).format(new Date());
        return "JPEG_" + timeStamp + "_" + Constants.JPG_EXTENSION;
    }

    private static File getDefaultPublicPictureStorage() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    }

    public static void getImageOrientation(String photoPath, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        String exifOrientation = ei
                .getAttribute(ExifInterface.TAG_ORIENTATION);
        Log.e("exifOrientation", "orientation = "+exifOrientation);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}
